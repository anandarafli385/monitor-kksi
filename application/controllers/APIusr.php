<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class APIusr extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data kontak
    function index_get() {
        $id = $this->get('id_user');
        if ($id == '') {
            $usr = $this->db->get('user')->result();
        } else {
            $this->db->where('id_user', $id);
            $usr = $this->db->get('user')->result();
        }
        $this->response($usr, 200);
    }


    //Mengirim atau menambah data kontak baru
    function index_post() {
        $data = array(
            'id_user'    => $this->input->post('id_user'),
            'nama'       => $this->input->post('nama'),
            'username'   => $this->input->post('username'),
            'password'   => $this->input->post('password'),
            'level'      => $this->input->post('level'));
        $insert = $this->db->insert('user', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('api_status' => 'fail', 502));
        }
    }

     //Memperbarui data kontak yang telah ada
    function index_put() {
        $id = $this->put('id_user');
        $data = array(
            'id_user'   => $this->input->put('id_user'),
            'nama'       => $this->input->put('nama'),
            'username'   => $this->input->put('username'),
            'password'   => $this->input->put('password'),
            'level'      => $this->input->put('level'));
        $this->db->where('id_user', $id);
        $update = $this->db->update('user', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('api_status' => 'fail', 502));
        }
    }

    //Menghapus salah satu data kontak
    function index_delete() {
        $id = $this->delete('id_user');
        $this->db->where('id_user', $id);
        $delete = $this->db->delete('user');
        if ($delete) {
            $this->response(array('api_status' => 'success'), 201);
        } else {
            $this->response(array('api_status' => 'fail', 502));
        }
    }
    
}
?>