<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class APIlog extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data kontak
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $usr = $this->db->get('userlog')->result();
        } else {
            $this->db->where('id', $id);
            $usr = $this->db->get('userlog')->result();
        }
        $this->response($usr, 200);
    }


    //Mengirim atau menambah data kontak baru
    function index_post() {
        $data = array(
            'id'            => $this->input->post('id'),
            'id_user'       => $this->input->post('id_user'),
            'userIp'        => $this->input->post('userIp'),
            'loginTime'     => $this->input->post('loginTime'));
        $insert = $this->db->insert('userlog', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('api_status' => 'fail', 502));
        }
    }

     //Memperbarui data kontak yang telah ada
    function index_put() {
        $id = $this->put('id');
        $data = array(
            'id'            => $this->input->post('id'),
            'id_user'       => $this->input->post('id_user'),
            'userIp'        => $this->input->post('userIp'),
            'loginTime'     => $this->input->post('loginTime'));
        $this->db->where('id', $id);
        $update = $this->db->update('userlog', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('api_status' => 'fail', 502));
        }
    }

    //Menghapus salah satu data kontak
    function index_delete() {
        $id = $this->delete('id');
        $this->db->where('id', $id);
        $delete = $this->db->delete('user');
        if ($delete) {
            $this->response(array('api_status' => 'success'), 201);
        } else {
            $this->response(array('api_status' => 'fail', 502));
        }
    }
    
}
?>