<?php
Class Rest_Supplier extends CI_Controller{
    
    var $API ="";
    
    function __construct() {
        parent::__construct();
        $this->API="http://localhost/monitor-kksi/APIsup";
        $this->load->library('curl');
        $this->load->model('Supplier_model');
    }
    
    // menampilkan data kontak
    function index(){
        $data['datasupp'] = json_decode($this->curl->simple_get($this->API.'/APIsup'));
        $this->load->view('includes/head-dash.php');
        $this->load->view('panel2/supplier',$data);
        $this->load->view('includes/foot-dash.php');
    }
    
    // insert data kontak
    function Add_Supp(){
        if(isset($_POST['submit'])){
            $data = array(
                    'kode_supplier'         => $this->post('kode_supplier'),
                    'nama_supplier'         => $this->post('nama_supplier'),
                    'alamat_supplier'       => $this->post('alamat_supplier'),
                    'telp_supplier'         => $this->post('telp_supplier'),
                    'kota_supplier'         => $this->post('kota_supplier'));
            $insert =  $this->curl->simple_post($this->API.'/APIsup', $data, array(CURLOPT_BUFFERSIZE => 10)); 
            if($insert)
            {
                $this->session->set_flashdata('hasil','Insert Data Berhasil');
            }else
            {
               $this->session->set_flashdata('hasil','Insert Data Gagal');
            }
            redirect('Rest_Supplier');
        }else{
            $this->load->view('includes/head-dash.php');
            $this->load->view('action/tambahSupplier');
            $this->load->view('includes/foot-dash.php');
        }
    }
    
    // edit data kontak
    function Edit_Supp($kode_supplier){
        if(isset($_POST['submit'])){
            $data = array(
                    'kode_supplier'         => $this->post('kode_supplier'),
                    'nama_supplier'         => $this->post('nama_supplier'),
                    'alamat_supplier'       => $this->post('alamat_supplier'),
                    'telp_supplier'         => $this->post('telp_supplier'),
                    'kota_supplier'         => $this->post('kota_supplier'));
            $update =  $this->curl->simple_put($this->API.'/APIsup', $data, array(CURLOPT_BUFFERSIZE => 10)); 
            if($update)
            {
                $this->session->set_flashdata('hasil','Update Data Berhasil');
            }else
            {
               $this->session->set_flashdata('hasil','Update Data Gagal');
            }
            redirect('Rest_Supplier');
        }else{
            $params = array('kode_supplier'=>  $this->uri->segment(4));
            $data['datasupp'] = json_decode($this->curl->simple_get($this->API.'/APIsup',$params));
            $data['supplier'] = $this->Supplier_model->getById($kode_supplier);
            $this->load->view('includes/head-dash');
            $this->load->view('action/editSupplier',$data);
            $this->load->view('includes/foot-dash');
        }
    }
    
    // delete data kontak
    function Delete_Supp($kode_supplier){
        if(empty($kode_supplier)){
            redirect('Rest_Supplier');
        }else{
            $delete =  $this->curl->simple_delete($this->API.'/APIsup', array('kode_supplier'=>$kode_supplier), array(CURLOPT_BUFFERSIZE => 10)); 
            if($delete)
            {
                $this->session->set_flashdata('hasil','Delete Data Berhasil');
            }else
            {
               $this->session->set_flashdata('hasil','Delete Data Gagal');
            }
            redirect('Rest_Supplier');
        }
    }

    function hapus_supplier($kode_supplier)
    {
        $this->Supplier_model->Delete($kode_supplier);
        redirect('Rest_Supplier');
    }

    function proses_tambah_supplier()
    {
        $this->Supplier_model->Add();
        redirect('Rest_Supplier');
    }

    function proses_edit_supplier()
    {
        $this->Supplier_model->Edit();
        redirect('Rest_Supplier');
    }

}