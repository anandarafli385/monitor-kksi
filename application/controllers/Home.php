<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index()
    {
        $this->load->view('includes/head-dash.php');
        $this->load->view('dashboard');
        $this->load->view('includes/foot-dash.php');
    }
}
