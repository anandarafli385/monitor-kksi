<?php
Class Rest_Barang extends CI_Controller{
    
    var $API ="";
    
    function __construct() {
        parent::__construct();
        $this->API="http://localhost/monitor-kksi/APIbar";
        $this->load->library('curl');
        $this->load->model('Barang_model');
    }
    
    // menampilkan data kontak
    function index(){
        $data['databar'] = json_decode($this->curl->simple_get($this->API.'/APIbar'));
        $this->load->view('includes/head-dash.php');
        $this->load->view('panel2/barang',$data);
        $this->load->view('includes/foot-dash.php');
    }
    
    // insert data kontak
    function Add_Bar(){
        if(isset($_POST['submit'])){
            $data = array(
                    'kode_barang'   => $this->input->post('kode_barang'),
                    'nama_barang'   => $this->input->post('nama_barang'),
                    'spesifikasi'   => $this->input->post('spesifikasi'),
                    'lokasi_barang' => $this->input->post('lokasi_barang'),
                    'kategori'      => $this->input->post('kategori'),
                    'total_barang'  => $this->input->post('total_barang'),
                    'kondisi'       => $this->input->post('kondisi'),
                    'jenis_barang'  => $this->input->post('jenis_barang'),
                    "sumber_dana"   => $this->input->post('sumber_dana'));
            $insert =  $this->curl->simple_post($this->API.'/APIbar', $data, array(CURLOPT_BUFFERSIZE => 10)); 
            if($insert)
            {
                $this->session->set_flashdata('hasil','Insert Data Berhasil');
            }else
            {
               $this->session->set_flashdata('hasil','Insert Data Gagal');
            }
            redirect('Rest_Barang');
        }else{
            $this->load->view('includes/head-dash.php');
            $this->load->view('action/tambahBarang');
            $this->load->view('includes/foot-dash.php');
        }
    }
    
    // edit data kontak
    function Edit_Bar($kode_barang){
        if(isset($_POST['submit'])){
            $data = array(
            'kode_barang'   => $this->input->post('kode_barang'),
            'nama_barang'   => $this->input->post('nama_barang'),
            'spesifikasi'   => $this->input->post('spesifikasi'),
            'lokasi_barang' => $this->input->post('lokasi_barang'),
            'kategori'      => $this->input->post('kategori'),
            'total_barang'  => $this->input->post('total_barang'),
            'kondisi'       => $this->input->post('kondisi'),
            'jenis_barang'  => $this->input->post('jenis_barang'),
            "sumber_dana"   => $this->input->post('sumber_dana'));
            $update =  $this->curl->simple_put($this->API.'/APIbar', $data, array(CURLOPT_BUFFERSIZE => 10)); 
            if($update)
            {
                $this->session->set_flashdata('hasil','Update Data Berhasil');
            }else
            {
               $this->session->set_flashdata('hasil','Update Data Gagal');
            }
            redirect('Rest_Barang');
        }else{
            $params = array('kode_barang'=>  $this->uri->segment(8));
            $data['databar'] = json_decode($this->curl->simple_get($this->API.'/APIbar',$params));
            $data['barang'] = $this->Barang_model->getById($kode_barang);
            $this->load->view('includes/head-dash');
            $this->load->view('action/editBarang',$data);
            $this->load->view('includes/foot-dash');
        }
    }
    
    // delete data kontak
    function Delete_Bar($kode_barang){
        if(empty($kode_barang)){
            redirect('Rest_Barang');
        }else{
            $delete =  $this->curl->simple_delete($this->API.'/APIbar', array('kode_barang'=>$kode_barang), array(CURLOPT_BUFFERSIZE => 10)); 
            if($delete)
            {
                $this->session->set_flashdata('hasil','Delete Data Berhasil');
            }else
            {
               $this->session->set_flashdata('hasil','Delete Data Gagal');
            }
            redirect('Rest_Barang');
        }
    }

    function hapus_barang($kode_barang)
    {
        $this->Barang_model->Delete($kode_barang);
        redirect('Rest_Barang');
    }

    function proses_tambah_barang()
    {
        $this->Barang_model->Add();
        redirect('Rest_Barang');
    }

    function proses_edit_barang()
    {
        $this->Barang_model->Edit();
        redirect('Rest_Barang');
    }

}