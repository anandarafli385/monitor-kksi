<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Security extends CI_Controller {

    public function index()
    {   
        $this->load->view('includes/head-dash');
        $this->load->view('security/all');
        $this->load->view('includes/foot-dash');
    }

    public function Network()
    {   
        $this->load->view('includes/head-dash');
        $this->load->view('security/network');
        $this->load->view('includes/foot-dash');
    }

    public function Events()
    {   
        $this->load->view('includes/head-dash');
        $this->load->view('security/events');
        $this->load->view('includes/foot-dash');
    }
    public function PCI()
    {   
        $this->load->view('includes/head-dash');
        $this->load->view('security/pci');
        $this->load->view('includes/foot-dash');
    }

    public function Simulation()
    {   
        $this->load->view('includes/head-dash');
        $this->load->view('security/simulation');
        $this->load->view('includes/foot-dash');
    }
}
