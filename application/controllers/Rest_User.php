<?php
Class Rest_User extends CI_Controller{
    
    var $API ="";
    var $LOG ="";
    
    function __construct() {
        parent::__construct();
        $this->API="http://localhost/monitor-kksi/APIusr";
        $this->LOG="http://localhost/monitor-kksi/APIlog";
        $this->load->library('curl');
        $this->load->model('User_model');
    }
    
    // menampilkan data kontak
    function index(){
        $data['datausr'] = json_decode($this->curl->simple_get($this->API.'/APIusr'));
        $this->load->view('includes/head-dash.php');
        $this->load->view('users/user',$data);
        $this->load->view('includes/foot-dash.php');
    }
    
    function User_Log(){
        $data['datalog'] = json_decode($this->curl->simple_get($this->API.'/APIlog'));
        $this->load->view('includes/head-dash.php');
        $this->load->view('users/log',$data);
        $this->load->view('includes/foot-dash.php');
    }

    // insert data kontak
    function Add_Usr(){
        if(isset($_POST['submit'])){
            $data = array(
            'id_user'    => $this->input->post('id_user'),
            'nama'       => $this->input->post('nama'),
            'username'   => $this->input->post('username'),
            'password'   => $this->input->post('password'),
            'level'      => $this->input->post('level'));
            $insert =  $this->curl->simple_post($this->API.'/APIusr', $data, array(CURLOPT_BUFFERSIZE => 10)); 
            if($insert)
            {
                $this->session->set_flashdata('hasil','Insert Data Berhasil');
            }else
            {
               $this->session->set_flashdata('hasil','Insert Data Gagal');
            }
            redirect('Rest_User');
        }else{
            $this->load->view('includes/head-dash.php');
            $this->load->view('action/tambahUser');
            $this->load->view('includes/foot-dash.php');
        }
    }
    
    // edit data kontak
    function Edit_Usr($id_user){
        if(isset($_POST['submit'])){
            $data = array(
            'id_user'    => $this->input->post('id_user'),
            'nama'       => $this->input->post('nama'),
            'username'   => $this->input->post('username'),
            'password'   => $this->input->post('password'),
            'level'      => $this->input->post('level'));
            $update =  $this->curl->simple_put($this->API.'/APIusr', $data, array(CURLOPT_BUFFERSIZE => 10)); 
            if($update)
            {
                $this->session->set_flashdata('hasil','Update Data Berhasil');
            }else
            {
               $this->session->set_flashdata('hasil','Update Data Gagal');
            }
            redirect('Rest_User');
        }else{
            $params = array('id_user'=>  $this->uri->segment(8));
            $data['datausr'] = json_decode($this->curl->simple_get($this->API.'/APIusr',$params));
            $data['user'] = $this->User_model->getById($id_user);
            $this->load->view('includes/head-dash');
            $this->load->view('action/editUser',$data);
            $this->load->view('includes/foot-dash');
        }
    }
    
    // delete data kontak
    function Delete_Usr($id_user){
        if(empty($id_user)){
            redirect('Rest_User');
        }else{
            $delete =  $this->curl->simple_delete($this->API.'/APIusr', array('id_user'=>$id_user), array(CURLOPT_BUFFERSIZE => 10)); 
            if($delete)
            {
                $this->session->set_flashdata('hasil','Delete Data Berhasil');
            }else
            {
               $this->session->set_flashdata('hasil','Delete Data Gagal');
            }
            redirect('Rest_User');
        }
    }

    function hapus_user($id_user)
    {
        $this->User_model->Delete($id_user);
        redirect('Rest_User');
    }

    function proses_tambah_user()
    {
        $this->User_model->Add();
        redirect('Rest_User');
    }

    function proses_edit_user()
    {
        $this->User_model->Edit();
        redirect('Rest_User');
    }

}