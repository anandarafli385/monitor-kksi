<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class APIbar extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data kontak
    function index_get() {
        $id = $this->get('kode_barang');
        if ($id == '') {
            $bar = $this->db->get('barang')->result();
        } else {
            $this->db->where('kode_barang', $id);
            $bar = $this->db->get('barang')->result();
        }
        $this->response($bar, 200);
    }


    //Mengirim atau menambah data kontak baru
    function index_post() {
        $data = array(
            'kode_barang'   => $this->input->post('kode_barang'),
            'nama_barang'   => $this->input->post('nama_barang'),
            'spesifikasi'   => $this->input->post('spesifikasi'),
            'lokasi_barang' => $this->input->post('lokasi_barang'),
            'kategori'      => $this->input->post('kategori'),
            'total_barang'  => $this->input->post('total_barang'),
            'kondisi'       => $this->input->post('kondisi'),
            'jenis_barang'  => $this->input->post('jenis_barang'),
            'sumber_dana'   => $this->input->post('sumber_dana'));
        $insert = $this->db->insert('barang', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('api_status' => 'fail', 502));
        }
    }

     //Memperbarui data kontak yang telah ada
    function index_put() {
        $id = $this->put('kode_barang');
        $data = array(
            'kode_barang'   => $this->input->put('kode_barang'),
            'nama_barang'   => $this->input->put('nama_barang'),
            'spesifikasi'   => $this->input->put('spesifikasi'),
            'lokasi_barang' => $this->input->put('lokasi_barang'),
            'kategori'      => $this->input->put('kategori'),
            'total_barang'  => $this->input->put('total_barang'),
            'kondisi'       => $this->input->put('kondisi'),
            'jenis_barang'  => $this->input->put('jenis_barang'),
            "sumber_dana"   => $this->input->put('sumber_dana'));
        $this->db->where('kode_barang', $id);
        $update = $this->db->update('barang', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('api_status' => 'fail', 502));
        }
    }

    //Menghapus salah satu data kontak
    function index_delete() {
        $id = $this->delete('kode_barang');
        $this->db->where('kode_barang', $id);
        $delete = $this->db->delete('barang');
        if ($delete) {
            $this->response(array('api_status' => 'success'), 201);
        } else {
            $this->response(array('api_status' => 'fail', 502));
        }
    }
    
}
?>