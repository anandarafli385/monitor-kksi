<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_model extends CI_Model 
{
	public function getAll()
	{
		return $this->db->get('barang')->result_array();
	}

	public function getById($kode_barang)
	{
		return $this->db->get_where('barang',['kode_barang'=>$kode_barang])->row_array();
	}

	public function Add()
	{
		$data = [
			"kode_barang" => $this->input->post('kode_barang'),
			"nama_barang" => $this->input->post('nama_barang'),
			"spesifikasi" => $this->input->post('spesifikasi'),
			"lokasi_barang" => $this->input->post('lokasi_barang'),
			"kategori" => $this->input->post('kategori'),
			"total_barang" => $this->input->post('total_barang'),
			"kondisi" => $this->input->post('kondisi'),
			"jenis_barang" => $this->input->post('jenis_barang'),
			"sumber_dana" => $this->input->post('sumber_dana'),
		];

		$this->db->insert('barang', $data);
	}

	public function Delete($kode_barang)
	{
		$this->db->where('kode_barang',$kode_barang);
		$this->db->delete('barang');
	}

	public function Edit()
	{
		$data = [
			"nama_barang" => $this->input->post('nama_barang'),
			"spesifikasi" => $this->input->post('spesifikasi'),
			"lokasi_barang" => $this->input->post('lokasi_barang'),
			"kategori" => $this->input->post('kategori'),
			"total_barang" => $this->input->post('total_barang'),
			"kondisi" => $this->input->post('kondisi'),
			"jenis_barang" => $this->input->post('jenis_barang'),
			"sumber_dana" => $this->input->post('sumber_dana'),		
		];

		$this->db->where('kode_barang',$this->input->post('kode_barang'));
		$this->db->update('barang',$data);
	}

}
