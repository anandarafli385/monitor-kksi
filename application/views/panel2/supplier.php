<main>
  <div id="container">
		<div class="card">
		  <div class="card-body">
        <a class="btn btn-info d-inline-flex mt-2 px-4 mx-2 mb-3" href="<?= base_url('Rest_Supplier')  ?>"><i class="nav-icon fas fa-box mr-2 mt-1"></i>Database Supplier</a>
        <a class="btn btn-outline-info d-inline-flex mt-2 px-4 mx-2 mb-3" href="<?= base_url('Rest_Barang')  ?>"><i class="nav-icon fas fa-box mr-2 mt-1"></i>Database Barang</a>
		  	<h4>Supplier Database</h4>
			<div class="float-left pb-3">
				<a class="btn btn-outline-info d-flex mt-2 px-4 mx-2" href="<?= base_url('APIsup')  ?>"><i class="nav-icon fas fa-code mr-2 mt-1"></i>Cek API Supplier</a>
        <a href="<?= base_url('Rest_Supplier/Add_supp')?>"  class="btn btn-outline-success d-flex mt-2 px-4 mx-2"><i class="nav-icon fas fa-plus mr-2 mt-1"></i>Tambah Supplier</a>
			</div>
			<div class="table-responsive">
			  <table class="table table-bordered">
				<thead>
				  <tr>
				    <td>No</td>
				    <td>Kode Supplier</td>
				    <td>Nama Supplier</td>
				    <td>Alamat Supplier</td>
				    <td>Telp Supplier</td>
            <td>Kota Supplier</td>
            <td>Setting</td>
				  </tr>
				</thead>
				<tbody>
          <?php 
          $no = 1;
          foreach ($datasupp as $supp){
          ?>
            <tr>
              <td><?= $no++ ?></td> 
              <td><?= $supp->kode_supplier ?></td> <!--Kode Supplier-->
              <td><?= $supp->nama_supplier ?></td> <!--Nama Supplier -->
              <td><?= $supp->alamat_supplier ?></td> <!--Lokasi Supplier -->
              <td><?= $supp->telp_supplier ?></td> <!-- Telp Supplier -->
              <td><?= $supp->kota_supplier ?></td> <!-- Kota Supplier -->
              <td>
                <div class="btn-group">
                  <a class="btn btn-outline-info" href="<?= base_url()?>Rest_Supplier/edit_supp/<?= $supp->kode_supplier ?>">Edit</a>
                  <a class="btn btn-outline-danger" href="<?= base_url()?>Rest_Supplier/delete_supp/<?= $supp->kode_supplier ?>" onclick="confirm('Apakah anda yakin menghapus data dengan nama <?= $supp->nama_supplier ?>?');">Hapus</a>
                </div>
              </td>
            </tr>
          <?php } ?>
				</tbody>
			  </table>
			</div>
		  </div>
		</div>
  </div>
</main>

<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark" style="background-color: #0B7479;">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <h3>Monitor Security Cloud & Pabrik/Warehouse</h3>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-5">
      <!-- Sidebar Menu -->
      <nav class="mt-5 pt-4 mx-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-closed">
              <a href="<?= base_url('Home')?>" class="mt-2 d-inline-flex nav-link">
                <i class="fa fa-home fa-2x"></i>
                <p class="h4 mt-1 ml-2">
                  Dashboard
                </p>
              </a>
            <div class="user-panel mb-2 "></div>
            <li class="nav-item">
            <a href="<?= base_url('Panel')  ?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p class="h4">
                Panel Sistem
              </p>
            </a>
            </li>
            <li class="nav-item">
            <a href="<?= base_url('Rest_Supplier')  ?>" class="nav-link active">
              <i class="nav-icon fas fa-code"></i>
              <p class="h4">
                Panel API
              </p>
            </a>
            </li>
            <li class="nav-item">
            <a href="<?= base_url('Rest_User')  ?>" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p class="h4">
                Manage User
              </p>
            </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-shield-alt"></i>
                <p class="h4">
                  Security
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= base_url('Security')?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p class="h5">All Events</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url('Security/Network')?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p class="h5">Network</p>
                    </a>
                  </li>
                  <li class="nav-item">
                  <a href="<?= base_url('Security/Events')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Security Events</p>
                  </a>
                  </li>
                  <li class="nav-item">
                  <a href="<?= base_url('Security/PCI')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Top PCI Event</p>
                  </a>
                 </li>
                 <li class="nav-item">
                  <a href="<?= base_url('Security/Simulation')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Simulation</p>
                  </a>
                 </li>
              </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    <!-- /.sidebar -->
  </aside>
</div>