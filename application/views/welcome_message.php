<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?= base_url()?>dashboard/plugins/fontawesome-free/css/all.min.css">
  <!-- datepicker -->
  <link rel="stylesheet" href="<?= base_url()?>dashboard/plugins/daterangepicker/daterangepicker.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url()?>dashboard/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url()?>dashboard/dist/css/adminlte.min.css">
<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body class="hold-transition sidebar-collapse sidebar-closed dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed mx-5 px-5 mt-5 pt-5">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark" style="background-color: #0B7479;">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <h3>Monitor Cloud & Pabrik/Warehouse</h3>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-5">
      <!-- Sidebar Menu -->
      <nav class="mt-5 pt-4 mx-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="<?= base_url('Home')  ?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p class="h4">
                Panel Sistem
              </p>
            </a>
            <a href="<?= base_url('Welcome')  ?>" class="nav-link active">
              <i class="nav-icon fas fa-code"></i>
              <p class="h4">
                Panel API
              </p>
            </a>
          </li>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
</div>
<div id="container">
	<h1 class="text-white">Selamat Datang di Database Suppliernya Rapli!</h1>

	<div id="body">
		<p>Ini Implementasi REST API ke CodeIgniter 3</p>

		<p><b>Klik dibawah ini untuk menampilkan API pada Tabel CRUD Supplier</b></p>
		<a href="<?= base_url('pakerest') ?>"><code>application/views/supplier.php</code></a>

		<p><b>Klik dibawah ini untuk menampilkan rawdata API</b></p>
		<a href="<?= base_url('APIsup') ?>"><code>application/controllers/Welcome.php</code></a>

		<p>Untuk mengecek fungsi API seperti <b>GET,PUT,POST,dan DELETE</b>,silahkan pakai <b>Postman</b> atau pakai <b>extension chrome ARC</b></p>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>


<script src="<?= base_url() ?>dashboard/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?= base_url() ?>dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?= base_url() ?>dashboard/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>dashboard/dist/js/adminlte.js"></script>
<!-- date-range-picker -->
<script src="<?= base_url() ?>dashboard/plugins/daterangepicker/daterangepicker.js"></script>
<!-- PAGE PLUGINS -->
<script src="<?= base_url() ?>dashboard/plugins/flot/jquery.flot.js"></script>
<!-- jQuery Mapael -->
<script src="<?= base_url() ?>dashboard/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="<?= base_url() ?>dashboard/plugins/raphael/raphael.min.js"></script>
<script src="<?= base_url() ?>dashboard/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="<?= base_url() ?>dashboard/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<script src="<?= base_url() ?>dashboard/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- ChartJS -->
<script src="<?= base_url() ?>dashboard/plugins/chart.js/Chart.min.js"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?= base_url() ?>dashboard/dist/js/pages/dashboard2.js"></script>
</body>
</html>