<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="row">
          <div class="col-md-12">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">All Events by Event Type
                <span class="badge text-secondary mr-5 pr-5">All Events by Event Type Last Update a few seconds Ago</span>
                </h3>
                <div class="card-tools">
                  <div class="btn-group my-2">
                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-wrench"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a href="#" class="dropdown-item">Action</a>
                      <a href="#" class="dropdown-item">Another action</a>
                      <a href="#" class="dropdown-item">Something else here</a>
                      <a class="dropdown-divider"></a>
                      <a href="#" class="dropdown-item">Separated link</a>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <div class="chart-responsive">
                      <canvas id="pieChart" height="120"></canvas>
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  <!-- /.col -->
                  <div class="col-md-6">
                    <div class="card direct-chat">
                      <div class="card-body"> 
                        <div class="direct-chat-messages container">
                          <!-- /.progress-group -->
                          <div class="progress-group">
                            UserLogon
                            <span class="float-right"><b>310</b>/400</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-danger" style="width: 75%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            <span class="progress-text">Visit Premium Page</span>
                            <span class="float-right"><b>480</b>/800</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-success" style="width: 60%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            PolicyScopeChange
                            <span class="float-right"><b>250</b>/500</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-warning" style="width: 50%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            UserLogoff
                            <span class="float-right"><b>160</b>/200</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-primary" style="width: 80%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            MachineLogon
                            <span class="float-right"><b>160</b>/200</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-info" style="width: 80%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            MachineLogoff
                            <span class="float-right"><b>160</b>/200</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-gray" style="width: 80%"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
              <!-- /.footer -->
            <!-- /.card -->
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="far fa-chart-bar"></i>
                  Logon Failures by Users
                  <span class="badge text-secondary mr-5 ">Failed Logon by DestinationAccount Last Update 5 Minute Ago</span>
                </h3>
                <div class="card-tools">
                  <div class="btn-group my-2">
                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-wrench"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a href="#" class="dropdown-item">Action</a>
                      <a href="#" class="dropdown-item">Another action</a>
                      <a href="#" class="dropdown-item">Something else here</a>
                      <a class="dropdown-divider"></a>
                      <a href="#" class="dropdown-item">Separated link</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="col-md-12">
                  <div class="chart-responsive">
                    <canvas id="barChart" height="270"></canvas>
                  </div>
                </div>
                <div class="col-md-12 mt-5">
                    <div class="card direct-chat">
                      <div class="card-body"> 
                        <div class="container-fluid">
                          <!-- /.progress-group -->
                          <div class="progress-group">
                            Success Logon
                            <span class="float-right"><b>160</b>/200</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-primary" style="width: 80%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            Failed Logon
                            <span class="float-right"><b>160</b>/200</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-gray" style="width: 80%"></div>
                            </div>
                          </div>
                        </div>
                        <div class="text-danger h4 mx-3 mt-3">Latest Suspicious Login</div>
                          <!-- /.card-header -->
                          <div class="card-body p-0">
                            <ul class="users-list clearfix">
                              <li>
                                <a class="users-list-name" href="#">Alexander Pierce</a>
                                <span class="users-list-date">Today</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Norman</a>
                                <span class="users-list-date">Yesterday</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">Jane</a>
                                <span class="users-list-date">12 Jan</span>
                              </li>
                              <li>
                                <a class="users-list-name" href="#">John</a>
                                <span class="users-list-date">12 Jan</span>
                              </li>
                            </ul>
                            <!-- /.users-list -->
                          </div>
                          <!-- /.card-body -->
                          <!-- /.card-footer -->
                      </div>
                    </div>
                  </div>
              </div>
              <!-- /.card-body-->
            </div>
          </div>
          <div class="col-md-6">
                <div class="card">
                  <div class="card-header border-transparent">
                    <h3 class="card-title">Latest Event Log
                      <br>
                    <span class="badge text-secondary mr-5">Event Log by EventType Last Update 5 Minute Ago</span>
                    </h3>
                    <div class="card-tools">
                      <div class="btn-group my-2">
                        <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                          <i class="fas fa-wrench"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                          <a href="#" class="dropdown-item">Action</a>
                          <a href="#" class="dropdown-item">Another action</a>
                          <a href="#" class="dropdown-item">Something else here</a>
                          <a class="dropdown-divider"></a>
                          <a href="#" class="dropdown-item">Separated link</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body p-0">
                    <div class="table-responsive mt-1 pb-2">
                      <table class="table m-0">
                        <thead>
                        <tr>
                          <th>User ID</th>
                          <th>User</th>
                          <th>Event Info</th>
                          <th>Detection Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR9842</a></td>
                          <td>Jean Doe</td>
                          <td><span class="badge badge-info">WebTrafficAudit</span></td>
                          <td>
                            <div class="sparkbar" data-color="#00a65a" data-height="20">2021-10-17 21:49:01</div>
                          </td>
                        </tr>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR1848</a></td>
                          <td>Bob Steel</td>
                          <td><span class="badge badge-warning">MachineLogon</span></td>
                          <td>
                            <div class="sparkbar" data-color="#f39c12" data-height="20">2021-10-17 21:49:01</div>
                          </td>
                        </tr>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR7429</a></td>
                          <td>Marlo Yeager</td>
                          <td><span class="badge badge-warning">MachineLogoff</span></td>
                          <td>
                            <div class="sparkbar" data-color="#f56954" data-height="20">2021-10-17 21:49:01</div>
                          </td>
                        </tr>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR7321</a></td>
                          <td>Annie Leonheart</td>
                          <td><span class="badge badge-danger">PolicyScopeChange</span></td>
                          <td>
                            <div class="sparkbar" data-color="#00c0ef" data-height="20">2021-10-17 21:49:01</div>
                          </td>
                        </tr>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR1488</a></td>
                          <td>Eren Yeager</td>
                          <td><span class="badge badge-info">ConfigurationTrafficAudit</span></td>
                          <td>
                            <div class="sparkbar" data-color="#f39c12" data-height="20">2021-10-17 21:49:01</div>
                          </td>
                        </tr>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR6518</a></td>
                          <td>Grisha Yeager</td>
                          <td><span class="badge badge-info">TCPTrafficAudit</span></td>
                          <td>
                            <div class="sparkbar" data-color="#f39c12" data-height="20">2021-10-17 21:49:01</div>
                          </td>
                        </tr>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR3328</a></td>
                          <td>Connie Spring</td>
                          <td><span class="badge badge-info">RegistryRead</span></td>
                          <td>
                            <div class="sparkbar" data-color="#f39c12" data-height="20">2021-10-17 21:49:01</div>
                          </td>
                        </tr>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR1238</a></td>
                          <td>Sasha Blause</td>
                          <td><span class="badge badge-warning">ServiceWarning</span></td>
                          <td>
                            <div class="sparkbar" data-color="#f39c12" data-height="20">2021-10-17 21:49:01</div>
                          </td>
                        </tr>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR1808</a></td>
                          <td>Levi Ackerman</td>
                          <td><span class="badge badge-warning">RegistryDelete</span></td>
                          <td>
                            <div class="sparkbar" data-color="#f39c12" data-height="20">2021-10-17 21:49:01</div>
                          </td>
                        </tr>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR7429</a></td>
                          <td>Mikasa Ackerman</td>
                          <td><span class="badge badge-danger">RegistryEdit</span></td>
                          <td>
                            <div class="sparkbar" data-color="#f56954" data-height="20">2021-10-17 21:49:01</div>
                          </td>
                        </tr>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR9842</a></td>
                          <td>Reinhardt</td>
                          <td><span class="badge badge-info">WebTrafficAudit</span></td>
                          <td>
                            <div class="sparkbar" data-color="#00a65a" data-height="20">2021-10-17 21:49:01</div>
                          </td>
                        </tr>
                        <tr>
                          <td><a href="pages/examples/invoice.html">OR1235</a></td>
                          <td>Zeke Yeager</td>
                          <td><span class="badge badge-danger">RegistryEdit</span></td>
                          <td>
                            <div class="sparkbar" data-color="#f56954" data-height="20">2021-10-17 21:49:01</div>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                </div>
                    <!-- /.table-responsive -->
                  </div>
                  <!-- /.card-body -->
                  <!-- /.card-footer -->
                </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="chart">
          <!-- Sales Chart Canvas -->
           <canvas id="salesChart" height="0" ></canvas>
         </div>
            <!-- /.row -->
            <!-- /.card -->

          <!-- /.col -->
      </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>


<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark" style="background-color: #0B7479;">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <h3>Monitor Security Cloud & Pabrik/Warehouse</h3>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-5">
      <!-- Sidebar Menu -->
      <nav class="mt-5 pt-4 mx-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-closed">
              <a href="<?= base_url('Home')?>" class="mt-2 d-inline-flex nav-link">
                <i class="fa fa-home fa-2x"></i>
                <p class="h4 mt-1 ml-2">
                  Dashboard
                </p>
              </a>
            <div class="user-panel mb-2 "></div>
            <li class="nav-item">
            <a href="<?= base_url('Panel')  ?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p class="h4">
                Panel Sistem
              </p>
            </a>
            </li>
            <li class="nav-item">
            <a href="<?= base_url('Rest_Supplier')  ?>" class="nav-link">
              <i class="nav-icon fas fa-code"></i>
              <p class="h4">
                Panel API
              </p>
            </a>
            </li>
            <li class="nav-item">
            <a href="<?= base_url('Rest_User')  ?>" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p class="h4">
                Manage User
              </p>
            </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link active">
                <i class="nav-icon fas fa-shield-alt"></i>
                <p class="h4">
                  Security
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= base_url('Security')?>" class="nav-link active">
                      <i class="far fa-circle nav-icon"></i>
                      <p class="h5">All Events</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url('Security/Network')?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p class="h5">Network</p>
                    </a>
                  </li>
                  <li class="nav-item">
                  <a href="<?= base_url('Security/Events')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Security Events</p>
                  </a>
                  </li>
                  <li class="nav-item">
                  <a href="<?= base_url('Security/PCI')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Top PCI Event</p>
                  </a>
                 </li>
                 <li class="nav-item">
                  <a href="<?= base_url('Security/Simulation')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Simulation</p>
                  </a>
                 </li>
              </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    <!-- /.sidebar -->
  </aside>
</div>