<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="row">
          <div class="col-md-6">
            <!-- interactive chart -->
            <div class="card card-primary card-outline pb-2">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="far fa-chart-bar"></i>
                  All Events - Real Time Data
                <span class="badge text-secondary mr-5 pr-5">All Events in 24/7 Last Update a few seconds Ago</span>
                </h3>
                <div class="card-tools my-2">
                  <div class="btn-group" id="realtime" data-toggle="btn-toggle">
                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-wrench"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a href="#" class="dropdown-item">Action</a>
                      <a href="#" class="dropdown-item">Another action</a>
                      <a href="#" class="dropdown-item">Something else here</a>
                      <a class="dropdown-divider"></a>
                      <a href="#" class="dropdown-item">Separated link</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body text-white mb-3">
                <div id="monitor" style="height: 300px;"></div>
              </div>
              <!-- /.card-body-->
            </div>
            <!-- /.card -->
          </div>
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title">Traffic by Destination Port
                  <br>
                <span class="badge text-secondary">All Network Traffic by DestinationPort Last Update 5 Minute Ago</span>
                </h5>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <div class="btn-group">
                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-wrench"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a href="#" class="dropdown-item">Action</a>
                      <a href="#" class="dropdown-item">Another action</a>
                      <a href="#" class="dropdown-item">Something else here</a>
                      <a class="dropdown-divider"></a>
                      <a href="#" class="dropdown-item">Separated link</a>
                    </div>
                  </div>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <p class="text-center">
                      <strong>Port Traffic: 1 October, 2021 - 30 October, 2021</strong>
                    </p>

                    <div class="chart">
                      <!-- Sales Chart Canvas -->
                      <canvas id="salesChart" height="180" style="height: 180px;"></canvas>
                    </div>
                    <!-- /.chart-responsive -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-12 mt-2">
                    <p class="text-center">
                      <strong>Port Accessed</strong>
                    </p>
                    <div class="container-fluid">
                      <div class="row">
                      <div class="col-md-6">
                        <div class="progress-group">
                          8080
                          <span class="float-right"><b>160</b>/200</span>
                          <div class="progress progress-sm">
                            <div class="progress-bar bg-primary" style="width: 80%"></div>
                          </div>
                        </div>
                        <!-- /.progress-group -->

                        <div class="progress-group">
                          80
                          <span class="float-right"><b>310</b>/400</span>
                          <div class="progress progress-sm">
                            <div class="progress-bar bg-danger" style="width: 75%"></div>
                          </div>
                        </div>
                      </div>  
                      <div class="col-md-6">
                          <!-- /.progress-group -->
                        <div class="progress-group">
                          <span class="progress-text">3000</span>
                          <span class="float-right"><b>480</b>/800</span>
                          <div class="progress progress-sm">
                            <div class="progress-bar bg-success" style="width: 60%"></div>
                          </div>
                        </div>

                        <!-- /.progress-group -->
                        <div class="progress-group">
                          4040
                          <span class="float-right"><b>250</b>/500</span>
                          <div class="progress progress-sm">
                            <div class="progress-bar bg-warning" style="width: 50%"></div>
                          </div>
                        </div>
                      </div>
                      </div>
                    </div>
                  </div>
                    <!-- /.progress-group -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- ./card-body -->
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
        </div>

        <!-- /.row -->
        <div class="row">
          <div class="col-md-6">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="far fa-chart-bar"></i>
                  Firewall Events by Type
                </h3>
                <span class="badge text-secondary mt-2">All Firewall by EventType Last 
                <br> Update 5 Minute Ago</span>
                <div class="card-tools">
                  <div class="btn-group my-2">
                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-wrench"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a href="#" class="dropdown-item">Action</a>
                      <a href="#" class="dropdown-item">Another action</a>
                      <a href="#" class="dropdown-item">Something else here</a>
                      <a class="dropdown-divider"></a>
                      <a href="#" class="dropdown-item">Separated link</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="col-md-12 mt-1">
                  <div class="chart-responsive">
                    <canvas id="pieChart" height="270"></canvas>
                  </div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="card direct-chat">
                      <div class="card-body"> 
                        <div class="direct-chat-messages container">
                          <!-- /.progress-group -->
                          <div class="progress-group">
                            DataBaseAccess
                            <span class="float-right"><b>310</b>/400</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-danger" style="width: 75%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            <span class="progress-text">Visit Premium Page</span>
                            <span class="float-right"><b>480</b>/800</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-success" style="width: 60%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            ApplicationDenial
                            <span class="float-right"><b>250</b>/500</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-warning" style="width: 50%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            ApplicationAccess
                            <span class="float-right"><b>160</b>/200</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-primary" style="width: 80%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            WebTrafficAudit
                            <span class="float-right"><b>160</b>/200</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-info" style="width: 80%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            UserAuthAudit
                            <span class="float-right"><b>160</b>/200</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-gray" style="width: 80%"></div>
                            </div>
                          </div>
                        </div>
                          
                      </div>
                    </div>
                  </div>
              </div>
              <!-- /.card-body-->
            </div>
          </div>
          <div class="col-md-6">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="far fa-chart-bar"></i>
                  All Events by Connector Name
                </h3>
                <span class="badge text-secondary mt-2">All Events by EventType Last
                <br> Update 5 Minute Ago</span>
                <div class="card-tools">
                  <div class="btn-group my-2">
                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-wrench"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a href="#" class="dropdown-item">Action</a>
                      <a href="#" class="dropdown-item">Another action</a>
                      <a href="#" class="dropdown-item">Something else here</a>
                      <a class="dropdown-divider"></a>
                      <a href="#" class="dropdown-item">Separated link</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <div class="col-md-12 mt-1">
                  <div class="chart-responsive">
                    <canvas id="donutChart1" height="270"></canvas>
                  </div>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="card direct-chat">
                      <div class="card-body"> 
                        <div class="direct-chat-messages container">
                          <!-- /.progress-group -->
                          <div class="progress-group">
                            Windows Security
                            <span class="float-right"><b>310</b>/400</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-danger" style="width: 75%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            <span class="progress-text">Visit Premium Page</span>
                            <span class="float-right"><b>480</b>/800</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-success" style="width: 60%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            FIM File and Directory
                            <span class="float-right"><b>250</b>/500</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-warning" style="width: 50%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            Windows System
                            <span class="float-right"><b>160</b>/200</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-primary" style="width: 80%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            Manager Monitor
                            <span class="float-right"><b>160</b>/200</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-info" style="width: 80%"></div>
                            </div>
                          </div>

                          <!-- /.progress-group -->
                          <div class="progress-group">
                            TriGeo
                            <span class="float-right"><b>160</b>/200</span>
                            <div class="progress progress-sm">
                              <div class="progress-bar bg-gray" style="width: 80%"></div>
                            </div>
                          </div>
                        </div>
                          
                      </div>
                    </div>
                  </div>
              </div>
              <!-- /.card-body-->
            </div>
          </div>
        </div>
          <!-- /.col -->
      </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>


<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark" style="background-color: #0B7479;">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <h3>Monitor Security Cloud & Pabrik/Warehouse</h3>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-5">
      <!-- Sidebar Menu -->
      <nav class="mt-5 pt-4 mx-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-closed">
              <a href="<?= base_url('Home')?>" class="mt-2 d-inline-flex nav-link">
                <i class="fa fa-home fa-2x"></i>
                <p class="h4 mt-1 ml-2">
                  Dashboard
                </p>
              </a>
            <div class="user-panel mb-2 "></div>
            <li class="nav-item">
            <a href="<?= base_url('Panel')  ?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p class="h4">
                Panel Sistem
              </p>
            </a>
            </li>
            <li class="nav-item">
            <a href="<?= base_url('Rest_Supplier')  ?>" class="nav-link">
              <i class="nav-icon fas fa-code"></i>
              <p class="h4">
                Panel API
              </p>
            </a>
            </li>
            <li class="nav-item">
            <a href="<?= base_url('Rest_User')  ?>" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p class="h4">
                Manage User
              </p>
            </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link active">
                <i class="nav-icon fas fa-shield-alt"></i>
                <p class="h4">
                  Security
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= base_url('Security')?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p class="h5">All Events</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url('Security/Network')?>" class="nav-link active">
                      <i class="far fa-circle nav-icon"></i>
                      <p class="h5">Network</p>
                    </a>
                  </li>
                  <li class="nav-item">
                  <a href="<?= base_url('Security/Events')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Security Events</p>
                  </a>
                  </li>
                  <li class="nav-item">
                  <a href="<?= base_url('Security/PCI')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Top PCI Event</p>
                  </a>
                 </li>
                 <li class="nav-item">
                  <a href="<?= base_url('Security/Simulation')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Simulation</p>
                  </a>
                 </li>
              </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    <!-- /.sidebar -->
  </aside>
</div>