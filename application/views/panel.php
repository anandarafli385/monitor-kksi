<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>MSC&PW</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="dashboard/plugins/fontawesome-free/css/all.min.css">
  <!-- datepicker -->
  <link rel="stylesheet" href="dashboard/plugins/daterangepicker/daterangepicker.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="dashboard/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dashboard/dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-collapse sidebar-closed dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed mx-5 px-5 mt-5 pt-5">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark" style="background-color: #0B7479;">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <h3>Monitor Security Cloud & Pabrik/Warehouse</h3>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-5">
      <!-- Sidebar Menu -->
      <nav class="mt-5 pt-4 mx-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-closed">
              <a href="<?= base_url('Home')?>" class="mt-2 d-inline-flex nav-link">
                <i class="fa fa-home fa-2x"></i>
                <p class="h4 mt-1 ml-2">
                  Dashboard
                </p>
              </a>
            <div class="user-panel mb-2 "></div>
            <li class="nav-item">
            <a href="<?= base_url('Panel')  ?>" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p class="h4">
                Panel Sistem
              </p>
            </a>
            </li>
            <li class="nav-item">
            <a href="<?= base_url('Rest_Supplier')  ?>" class="nav-link">
              <i class="nav-icon fas fa-code"></i>
              <p class="h4">
                Panel API
              </p>
            </a>
            </li>
            <li class="nav-item">
            <a href="<?= base_url('Rest_User')  ?>" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p class="h4">
                Manage User
              </p>
            </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-shield-alt"></i>
                <p class="h4">
                  Security
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= base_url('Security')?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p class="h5">All Events</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url('Security/Network')?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p class="h5">Network</p>
                    </a>
                  </li>
                  <li class="nav-item">
                  <a href="<?= base_url('Security/Events')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Security Events</p>
                  </a>
                  </li>
                  <li class="nav-item">
                  <a href="<?= base_url('Security/PCI')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Top PCI Event</p>
                  </a>
                 </li>
                 <li class="nav-item">
                  <a href="<?= base_url('Security/Simulation')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Simulation</p>
                  </a>
                 </li>
              </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    <!-- /.sidebar -->
  </aside>
</div>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper mt-5">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-lg-12">
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-4">
                  <div class="card border border-secondary rounded-0 pb-5">
                    <div class="card-body">
                      <div class="row justify-content-between">
                        <h5>Status Mesin</h5>
                        <h5 class="text-danger"><b>OFF</b></h5>
                      </div>
                      <div class="text-center">Ultilization</div>
                      <div class="text-center">
                        <input type="text" class="knob text-white" value="NaN" data-width="149" data-height="149"data-fgColor="#f02c05">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="card border border-secondary rounded-0 pb-3">
                    <div class="card-body">
                      <h4 class="text-teal">
                        Data Basic
                      </h4>
                      <div class="container-fluid ml-2 pt-4">
                        <div class="container">
                          <div class="row justify-content-between">
                            <div class="col-6 col-md-6 px-0">
                              <small class="h6 px-0">Uptime Hours</small>
                              <h6 class="ml-4 pl-3 text-yellow"><b>0 H</b></h6>
                            </div>
                            <div class="col-6 col-md-6 px-0">
                              <small class="h6 px-0">Uptime Minutes</small>
                              <h6 class="ml-4 pl-3 text-yellow"><b>0 M</b></h6>
                            </div>
                          </div>
                        </div>
                        <div class="container pt-3">
                          <div class="row justify-content-between">
                            <div class="col-6 col-md-6 px-0">
                              <small class="h6 px-0">Runtime Hours</small>
                              <h6 class="ml-4 pl-3 text-yellow"><b>0 H</b></h6>
                            </div>
                            <div class="col-6 col-md-6 px-0">
                              <small class="h6 px-0">Runtime Minutes</small>
                              <h6 class="ml-4 pl-3 text-yellow"><b>0 M</b></h6>
                            </div>
                          </div>
                        </div>
                        <div class="container pt-3">
                          <div class="row justify-content-between">
                            <div class="col-6 col-md-6 px-0">
                              <small class="h6 px-0">Downtime Hours</small>
                              <h6 class="ml-4 pl-3 text-yellow"><b>0 H</b></h6>
                            </div>
                            <div class="col-6 col-md-6 px-0">
                              <small class="h6 px-0">Downtime Minutes</small>
                              <h6 class="ml-4 pl-3 text-yellow"><b>0 M</b></h6>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="card border border-secondary rounded-0">
                    <div class="card-body">
                      <h4 class="text-teal">
                        Cost
                      </h4>
                      <h5 class="text-center">Cost Today</h5>
                      <h1 class="text-center"><b><p class="h1">RP. 0</p></b></h1>
                        <div class="container-fluid ml-2 mt-2 pt-1">
                          <div class="container">
                            <div class="row justify-content-between">
                              <div class="col-5 col-md-5">
                                <h6 class="text-center">Total Cost</h6>
                                <h6 class="text-center"><b>Rp.0</b></h6>
                              </div>
                              <div class="col-6 col-md-6">
                                <h6 class="text-center">Energy Total</h6>
                                <h6 class="text-center"><b>0 kWh</b></h6>
                              </div>
                            </div>
                          </div>
                          <div class="container pt-4">
                            <div class="row justify-content-between">
                              <div class="col-5 col-md-5">
                                <h6 class="text-center">Power Total</h6>
                                <h6 class="text-center"><b>95.1 watt</b></h6>
                              </div>
                              <div class="col-6 col-md-6">
                                <h6 class="text-center">Arus Total</h6>
                                <h6 class="text-center"><b>0.91 A</b></h6>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <div class="container-fluid">
              <div class="row">
                <div class="col-lg-4">
                  <div class="card border border-secondary rounded-0 px-3 py-3">
                    <div class="card-body" style="height: 380px">
                      <h1 class="text-center mt-5 pt-5" style="font-size:50pt"><b>Frekuensi</b></h1>
                      <h2 class="text-center mt-5" style="font-size: 35pt">70 Hz</h2>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="card border border-secondary rounded-0 px-3 py-3">
                    <h4 class="text-teal">Energy Total</h4>
                      <div class="container pt-4">
                        <div class="row justify-content-center">
                          <h4 class="text-center">Energy Total</h4>
                          <h4 class="text-purple ml-2"> 0 kWh</h4>
                        </div>
                      </div>
                    <div class="card-body" id="interactive7" style="height: 283px;">
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="card border border-secondary rounded-0 px-3 py-3">
                    <h4 class="text-teal">Power Total</h4>
                      <div class="container pt-4">
                        <div class="row justify-content-center">
                          <h4 class="text-center">Power Total</h4>
                          <h4 class="text-purple ml-2"> 95.1 watt</h4>
                        </div>
                      </div>
                    <div class="card-body" id="interactive6" style="height: 283px;">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-4">
                  <div class="card border border-secondary rounded-0 px-3 py-3">
                    <h4 class="text-teal">Power Factor</h4>
                      <div class="row justify-content-center">
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">R</h6>
                              <h6 class="text-center ml-2" style="color:#f21f6d"> 0.45</h6>
                            </div>
                          </div>
                        </div>
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">S</h6>
                              <h6 class="text-center ml-2" style="color:#f21f6d"> 0.33</h6>
                            </div>
                          </div>
                        </div>
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">T</h6>
                              <h6 class="text-center ml-2" style="color:#f21f6d"> 0.46</h6>
                            </div>
                          </div>
                        </div>
                      </div>
                    <div class="card-body" id="interactive5" style="height: 283px;">
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="card border border-secondary rounded-0 px-3 py-3">
                    <h4 class="text-teal">Arus</h4>
                      <div class="row justify-content-center">
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">R</h6>
                              <h6 class="text-center ml-2" style="color:#f21f6d"> 0.45 A</h6>
                            </div>
                          </div>
                        </div>
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">S</h6>
                              <h6 class="text-center ml-2" style="color:#f21f6d"> 0.33 A</h6>
                            </div>
                          </div>
                        </div>
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">T</h6>
                              <h6 class="text-center ml-2" style="color:#f21f6d"> 0.46 A</h6>
                            </div>
                          </div>
                        </div>
                      </div>
                    <div class="card-body" id="interactive4" style="height: 283px;">
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="card border border-secondary rounded-0 px-3 py-3">
                    <h4 class="text-teal">Phase R</h4>
                      <div class="row justify-content-center">
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">Tegangan</h6>
                              <h6 class="text-center ml-2" style="color:#00ff80"> 221,30 V</h6>
                            </div>
                          </div>
                        </div>
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">Arus</h6>
                              <h6 class="text-center ml-2" style="color:#00ff80"> 0.33 A</h6>
                            </div>
                          </div>
                        </div>
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">P Factor</h6>
                              <h6 class="text-center ml-2" style="color:#00ff80"> 0.46</h6>
                            </div>
                          </div>
                        </div>
                      </div>
                    <div class="card-body" id="interactive3" style="height: 283px;">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-4">
                  <div class="card border border-secondary rounded-0 px-3 py-3">
                    <h4 class="text-teal">Phase S</h4>
                      <div class="row justify-content-center">
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">Tegangan</h6>
                              <h6 class="text-center ml-2" style="color:#00ff80"> 221,30 V</h6>
                            </div>
                          </div>
                        </div>
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">Arus</h6>
                              <h6 class="text-center ml-2" style="color:#00ff80"> 0.33 A</h6>
                            </div>
                          </div>
                        </div>
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">P Factor</h6>
                              <h6 class="text-center ml-2" style="color:#00ff80"> 0.46</h6>
                            </div>
                          </div>
                        </div>
                      </div>
                    <div class="card-body" id="interactive2" style="height: 283px;">
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="card border border-secondary rounded-0 px-3 py-3">
                    <h4 class="text-teal">Phase T</h4>
                      <div class="row justify-content-center">
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">Tegangan</h6>
                              <h6 class="text-center ml-2" style="color:#00ff80"> 221,30 V</h6>
                            </div>
                          </div>
                        </div>
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">Arus</h6>
                              <h6 class="text-center ml-2" style="color:#00ff80"> 0.33 A</h6>
                            </div>
                          </div>
                        </div>
                        <div class="col-4 col-md-4">
                          <div class="container pt-4">
                            <div class="justify-content-center">
                              <h6 class="text-center">P Factor</h6>
                              <h6 class="text-center ml-2" style="color:#00ff80"> 0.46</h6>
                            </div>
                          </div>
                        </div>
                      </div>
                    <div class="card-body d-block" id="interactive8" style="height: 283px;">
                    </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="card border border-secondary rounded-0">
                    <div class="card-body">
                      <h4 class="text-teal">
                        Data History Information
                      </h4>
                      <div class="form-group">
                        <label>Date:</label>
                          <div class="input-group date"  id="reservationdate" data-target-input="nearest">
                              <input type="date" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                              <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                              </div>
                          </div>
                      </div>
                      <a href="<?= base_url('Panel/excel')  ?>"><div class="btn btn-success btn-block text-white">Data History Information</div></a>
                    </div>
                  </div>
                </div>
              </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">


        <!-- Main row -->
        <div class="row">

        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="<?= base_url() ?>dashboard/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?= base_url() ?>dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?= base_url() ?>dashboard/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>dashboard/dist/js/adminlte.js"></script>
<!-- date-range-picker -->
<script src="<?= base_url() ?>dashboard/plugins/daterangepicker/daterangepicker.js"></script>
<!-- PAGE PLUGINS -->
<script src="<?= base_url() ?>dashboard/plugins/flot/jquery.flot.js"></script>
<!-- jQuery Mapael -->
<script src="<?= base_url() ?>dashboard/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="<?= base_url() ?>dashboard/plugins/raphael/raphael.min.js"></script>
<script src="<?= base_url() ?>dashboard/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="<?= base_url() ?>dashboard/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<script src="<?= base_url() ?>dashboard/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- ChartJS -->
<script src="<?= base_url() ?>dashboard/plugins/chart.js/Chart.min.js"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?= base_url() ?>dashboard/dist/js/pages/dashboard2.js"></script>
<script src="<?= base_url() ?>chart/realtime.js"></script>
<script>
  $(function () {
    /* jQueryKnob */

    $('.knob').knob({
      /*change : function (value) {
       //console.log("change : " + value);
       },
       release : function (value) {
       console.log("release : " + value);
       },
       cancel : function () {
       console.log("cancel : " + this.value);
       },*/
      draw: function () {

        // "tron" case
        if (this.$.data('skin') == 'tron') {

          var a   = this.angle(this.cv)  // Angle
            ,
              sa  = this.startAngle          // Previous start angle
            ,
              sat = this.startAngle         // Start angle
            ,
              ea                            // Previous end angle
            ,
              eat = sat + a                 // End angle
            ,
              r   = true

          this.g.lineWidth = this.lineWidth

          this.o.cursor
          && (sat = eat - 0.3)
          && (eat = eat + 0.3)

          if (this.o.displayPrevious) {
            ea = this.startAngle + this.angle(this.value)
            this.o.cursor
            && (sa = ea - 0.3)
            && (ea = ea + 0.3)
            this.g.beginPath()
            this.g.strokeStyle = this.previousColor
            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false)
            this.g.stroke()
          }

          this.g.beginPath()
          this.g.strokeStyle = r ? this.o.fgColor : this.fgColor
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false)
          this.g.stroke()

          this.g.lineWidth = 2
          this.g.beginPath()
          this.g.strokeStyle = this.o.fgColor
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false)
          this.g.stroke()

          return false
        }
      }
    })
    /* END JQUERY KNOB */


    var sparkline2 = new Sparkline($('#sparkline-2')[0], { width: 240, height: 70, lineColor: '#f02c05', endColor: '#f02c05' })
    sparkline2.draw([515, 519, 520, 522, 652, 810, 370, 627, 319, 630, 921])
  })
</script>
</body>
</html>
