<script src="<?= base_url() ?>dashboard/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?= base_url() ?>dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?= base_url() ?>dashboard/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>dashboard/dist/js/adminlte.js"></script>
<!-- date-range-picker -->
<script src="<?= base_url() ?>dashboard/plugins/daterangepicker/daterangepicker.js"></script>
<!-- PAGE PLUGINS -->
<script src="<?= base_url() ?>dashboard/plugins/flot/jquery.flot.js"></script>
<!-- jQuery Mapael -->
<script src="<?= base_url() ?>dashboard/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="<?= base_url() ?>dashboard/plugins/raphael/raphael.min.js"></script>
<script src="<?= base_url() ?>dashboard/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="<?= base_url() ?>dashboard/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<script src="<?= base_url() ?>dashboard/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- ChartJS -->
<script src="<?= base_url() ?>dashboard/plugins/chart.js/Chart.min.js"></script>
<script src="<?= base_url() ?>chart/realtime.js"></script>
<script src="<?= base_url() ?>chart/main.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
</body>
</html>    