<html lang="en">
<head>
  <meta charset="utf-8">
   <meta name="dicoding:email" content="anandarafli385@gmail.com">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>MSC&PW</title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?= base_url()?>dashboard/plugins/fontawesome-free/css/all.min.css">
  <!-- datepicker -->
  <link rel="stylesheet" href="<?= base_url()?>dashboard/plugins/daterangepicker/daterangepicker.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url()?>dashboard/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url()?>dashboard/dist/css/adminlte.min.css">
</head>
<body class="hold-transition sidebar-collapse sidebar-closed dark-mode sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed mx-5 px-5 mt-5 pt-5">