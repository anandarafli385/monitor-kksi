<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Data History Information</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?= base_url() ?>dashboard/plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>DataTables/datatables.min.css"/>
  <link rel="stylesheet" href="<?= base_url() ?>DataTables/Buttons-2.0.0/css/buttons.dataTables.min.css"></link>
  <link rel="stylesheet" type="text/css" href="<?= base_url() ?>DataTables/DataTables-1.11.2/css/jquery.dataTables.min.css"/>
  <link rel="stylesheet" href="<?= base_url() ?>vendor/bootstrap.min.css"></link>
</head>
<body>
	<div class="container my-5">
		<center class="mb-5">
			<br>
			<br>
			<h1 style="color: green;">Data History Information</h1>
		</center>

		<table id="database" class="table table-striped table-bordered display">
		        <thead>
		            <tr>
		            	<th scope="col">No.</th>
		                <th scope="col">Date</th>
		                <th scope="col">Cost</th>
		                <th scope="col">Total Energy</th>
		            </tr>
		        </thead>
		        <tbody>
		        	<tr>
		            	<td scope="row">1</td>
		                <td>2021/09/17</td>
		                <td>Rp. 11500</td>
		                <td>7.66 kWh</td>
		        	</tr>
		        	<tr><td scope="row">2</td>
		                <td>2021/04/17</td>
		                <td>Rp. 16500</td>
		                <td>12.66 kWh</td>
		        	</tr>
		        	<tr>
		            	<td scope="row">3</td>
		                <td>2021/09/17</td>
		                <td>Rp. 13500</td>
		                <td>10.5 kWh</td>
		        	</tr>
		        	<tr>
		            	<td scope="row">4</td>
		                <td>2021/09/17</td>
		                <td>Rp. 12500</td>
		                <td>8.66 kWh</td>
		        	</tr>
		        	<tr>
		            	<td scope="row">5</td>
		                <td>2021/09/17</td>
		                <td>Rp. 12500</td>
		                <td>8.66 kWh</td>
		        	</tr>
		        	<tr>
		            	<td scope="row">6</td>
		                <td>2021/09/17</td>
		                <td>Rp. 12500</td>
		                <td>8.66 kWh</td>
		        	</tr>
		        	<tr>
		            	<td scope="row">7</td>
		                <td>2021/09/17</td>
		                <td>Rp. 12500</td>
		                <td>8.66 kWh</td>
		        	</tr>
		        	<tr>
		            	<td scope="row">8</td>
		                <td>2021/09/17</td>
		                <td>Rp. 12500</td>
		                <td>8.66 kWh</td>
		        	</tr>
		        	<tr>
		            	<td scope="row">9</td>
		                <td>2021/09/17</td>
		                <td>Rp. 12500</td>
		                <td>8.66 kWh</td>
		        	</tr>
		        	<tr>
		            	<td scope="row">10</td>
		                <td>2021/09/17</td>
		                <td>Rp. 12500</td>
		                <td>8.66 kWh</td>
		        	</tr>
		        	<tr>
		            	<td scope="row">11</td>
		                <td>2021/09/17</td>
		                <td>Rp. 12500</td>
		                <td>8.66 kWh</td>
		        	</tr>
		        	<tr>
		            	<td scope="row">12</td>
		                <td>2021/09/17</td>
		                <td>Rp. 12500</td>
		                <td>8.66 kWh</td>
		        	</tr>
		        	<tr>
		            	<td scope="row">13</td>
		                <td>2021/09/17</td>
		                <td>Rp. 12500</td>
		                <td>8.66 kWh</td>
		        	</tr>
		        </tbody>  
		</table>
	</div>



<script type="text/javascript" src="<?=base_url() ?>dashboard/plugins/jquery/jquery.js"></script>
<script type="text/javascript" src="<?=base_url() ?>DataTables/datatables.min.js"></script>
<script type="text/javascript" src="<?=base_url() ?>DataTables/Buttons-2.0.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?=base_url() ?>DataTables/JSZip-2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="<?=base_url() ?>DataTables/Buttons-2.0.0/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?=base_url() ?>vendor/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
    $('#database').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'excelHtml5',
        ]
    } );
} );
</script>
</body>
</html>