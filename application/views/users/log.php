	<main>
	  <div class="container-fluid">
		<div class="card">
		  <div class="card-body">
		  	<h4>User Database</h4>
			<div class="float-left pb-3">
				<a class="btn btn-outline-info d-flex mt-2 px-4 mx-2" href="<?= base_url('APIusr')?>"><i class="nav-icon fas fa-code mr-2 mt-1"></i>Cek API User</a>
        <a class="btn btn-outline-info d-flex mt-2 px-4 mx-2" href="<?= base_url('Rest_User')?>"><i class="nav-icon fas fa-user mr-2 mt-1"></i>Database User</a>
        <a href="<?= base_url('Rest_User/Add_usr')?>"  class="btn btn-outline-success d-flex mt-2 px-4 mx-2"><i class="nav-icon fas fa-plus mr-2 mt-1"></i>Tambah Userr</a>
			</div>
						<div class="table-responsive">
							<table class="table table-bordered table-hover">
								<thead class="text-center">
									<tr>
										<td>NO</td>
										<td>Nama Pengguna</td>
										<td>Username</td>
										<td>IP</td>
										<td>Waktu Login</td>
									</tr>
								</thead>
								<tbody>
									<?php
									$connect = new mysqli("localhost", "root", "", "inventory");
									if ($connect->connect_error) {
									  die("Gagal" . $connect->connect_error);
									}
									else {
									  // echo "Koneksi Berhasil";
									}
									 ?>

									<?php 
										$no = 1;
										$query = mysqli_query($connect, "SELECT user.nama, user.username, userlog.userIp, userlog.loginTime FROM user, userlog WHERE user.id_user = userlog.id_user");
										if (mysqli_num_rows($query) > 0) {
											while ($row = mysqli_fetch_array($query)) {
													?>
													<tr>
														<td><?=$no++?></td> <!-- Kode Supplier -->
														<td><?=$row[0]?></td> <!-- Nama Supllier -->
														<td><?=$row[1]?></td> <!-- Telp Supllier -->
														<td><?=$row[2]?></td> <!-- Kota Supllier -->
														<td><?=$row[3]?></td> <!-- Alamat Supllier -->
													</tr>
													<?php
											}		
										}
										else {
											?>
											<tr>
												<td class="text-center" colspan="6">
													<h1>-- Tidak ada data --</h1>
												</td>
											</tr>
											<?php
										}
									?>
								</tbody>
							</table>	
						</div>									
		  </div>
		</div>
	  </div>
	</main>


<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark" style="background-color: #0B7479;">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <h3>Monitor Security Cloud & Pabrik/Warehouse</h3>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-5">
      <!-- Sidebar Menu -->
      <nav class="mt-5 pt-4 mx-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-closed">
              <a href="<?= base_url('Home')?>" class="mt-2 d-inline-flex nav-link">
                <i class="fa fa-home fa-2x"></i>
                <p class="h4 mt-1 ml-2">
                  Dashboard
                </p>
              </a>
            <div class="user-panel mb-2 "></div>
            <li class="nav-item">
            <a href="<?= base_url('Panel')  ?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p class="h4">
                Panel Sistem
              </p>
            </a>
            </li>
            <li class="nav-item">
            <a href="<?= base_url('Rest_Supplier')  ?>" class="nav-link">
              <i class="nav-icon fas fa-code"></i>
              <p class="h4">
                Panel API
              </p>
            </a>
            </li>
            <li class="nav-item">
            <a href="<?= base_url('Rest_User')  ?>" class="nav-link active">
              <i class="nav-icon fas fa-user"></i>
              <p class="h4">
                Manage User
              </p>
            </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-shield-alt"></i>
                <p class="h4">
                  Security
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= base_url('Security')?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p class="h5">All Events</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url('Security/Network')?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p class="h5">Network</p>
                    </a>
                  </li>
                  <li class="nav-item">
                  <a href="<?= base_url('Security/Events')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Security Events</p>
                  </a>
                  </li>
                  <li class="nav-item">
                  <a href="<?= base_url('Security/PCI')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Top PCI Event</p>
                  </a>
                 </li>
                 <li class="nav-item">
                  <a href="<?= base_url('Security/Simulation')?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p class="h5">Simulation</p>
                  </a>
                 </li>
              </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    <!-- /.sidebar -->
  </aside>
</div>