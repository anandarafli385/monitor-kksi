# Dummy SaaS & Cloud Networking KKSI NEMTAS 

_Web Aplikasi ini dibuat menggunakan framework CodeIgniter 3 dengan konsep MVC(Model,View,Controller)._

**Library :**
1. CI REST Server : https://github.com/ardisaurus/ci-restserver
2. Library CURL : https://github.com/ardisaurus/ci-curl
3. AdminLTE : https://adminlte.io/
4. DataTables : https://datatables.net/

**Tools :**
1. XAMPP(Recommend PHP 7.4) = For Web Server
2. Sublime Text/VSCODE = For Text Editor

**_Silahkan Pilih Opsi Instalasi Anda :_**

**Untuk menggunakan aplikasi ini silahkan :** 

**_Setelah selesai di clone/download rename folder jadi_**`materi-kksi` 

**A. Clone via Git**
1. Clone project ini via gitlab ke direktori xampp/htdocs anda
2. Lalu buka folder yang anda clone
3. cari folder `database` yang berisi file database `inventory.sql`
4. Nyalakan modul `Apache` dan `MySQL` anda di XAMPP
5. lalu,buka `PHPmyAdmin` anda di browser,lalu buat database dengan nama `inventory`
6. pada halaman database `inventory` silahkan klik import lalu import database `inventory.sql`
7. setelah selesai import,silahkan jalankan aplikasi ini melalui web browser anda dengan cara berikut : 
	**ketik http://localhost/monitor-kksi di browser anda**


**B. Download**
1. Download dalam bentuk ZIP
2. Extract folder kedalam direktori xampp/htdocs anda
3. cari folder `database` yang berisi file database `inventory.sql`
4. Nyalakan modul `Apache` dan `MySQL` anda di XAMPP
5. lalu,buka `PHPmyAdmin` anda di browser,lalu buat database dengan nama `inventory`
6. pada halaman database `inventory` silahkan klik import lalu import database `inventory.sql`
7. setelah selesai import,silahkan jalankan aplikasi ini melalui web browser anda dengan cara berikut :
	**ketik http://localhost/monitor-kksi di browser anda**
